﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 100; i++)
            {
                GetFizzBuzz(i);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Returns Fizz/Buzz or another value if entered into dictionary
        /// </summary>
        /// <param name="i">Current number</param>
        private static void GetFizzBuzz(int i)
        {
            var numberDictionary = GetDictionaryItems();

            string response = string.Empty;

            foreach (var number in numberDictionary)
            {
                if (i % number.Key == 0)
                {
                    response += number.Value;
                }
            }

            //not a multiple, return number
            if (response.Length == 0)
            {
                response = i.ToString();
            }

          Console.WriteLine(response);
        }


        /// <summary>
        /// Sets up the Dictionary Items
        /// </summary>
        /// <returns>Dictionary items of numbers to check</returns>
        private static Dictionary<int, string> GetDictionaryItems()
        {
            //set up pairs - can't have duplicates
            return new Dictionary<int, string>
            {
                { 3, "Fizz" },
                { 5, "Buzz" },
                { 15, "Boom" }
            };
        }

    }
}
